package com.id.taqi.crud.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Author {

	@Id @GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid", strategy="uuid2")
	@Column(name = "id_author")
	private String idAuthor;
	
	@Column(name = "nama_author")
	private String namaAuthor;
	
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_author",referencedColumnName = "id_author")
	private List<Article> articles;
	public Author() {
		// TODO Auto-generated constructor stub
	}
	public Author(String idAuthor, String namaAuthor, String email, List<Article> articles) {
		super();
		this.idAuthor = idAuthor;
		this.namaAuthor = namaAuthor;
		this.email = email;
		this.articles = articles;
	}
	public String getIdAuthor() {
		return idAuthor;
	}
	public void setIdAuthor(String idAuthor) {
		this.idAuthor = idAuthor;
	}
	public String getNamaAuthor() {
		return namaAuthor;
	}
	public void setNamaAuthor(String namaAuthor) {
		this.namaAuthor = namaAuthor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}	
}
