package com.id.taqi.crud.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.crud.common.BaseException;
import com.id.taqi.crud.dao.AuthorDao;
import com.id.taqi.crud.model.Author;

import io.swagger.annotations.Api;

@Api(tags = "Modul Author")
@RestController @RequestMapping("/api")
public class AuthorController {

	@Autowired
	AuthorDao authorDao;
	
	@GetMapping("/author")
	public Page<Author> getAll(Pageable pageable){
		return authorDao.findAll(pageable);
	}
	
	@GetMapping("/author/{idAuthor}")
	public Author getById(@PathVariable Author author){
		return author;
	}
	
	@PostMapping("/author")
	public Author createAuthor(@Valid @RequestBody Author author){
		return authorDao.save(author);
	}
	
	@PutMapping("/author/{idAuthor}")
	public Author updateAuthor(@PathVariable(value = "idAuthor") String idAuthor,@Valid @RequestBody Author author){
		Author objAuthor = authorDao.findById(idAuthor).orElseThrow(() -> new BaseException("Author dengan ", "ID Author = ", idAuthor));
		objAuthor.setNamaAuthor(author.getNamaAuthor());
		objAuthor.setEmail(author.getEmail());
		
		Author a = authorDao.save(objAuthor);
		return a;
	}
	
	@DeleteMapping("/author")
	public ResponseEntity<?> deleteAuthor(@PathVariable(value = "idAuthor") String idAuthor){
		Author objAuthor = authorDao.findById(idAuthor).orElseThrow(() -> new BaseException("Author dengan ", "ID Author = ", idAuthor));
		authorDao.delete(objAuthor);
		return ResponseEntity.ok().build();
	}
}
