package com.id.taqi.crud.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.crud.dao.PersonDao;
import com.id.taqi.crud.model.Person;

@RestController @RequestMapping("/api")
//@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class PersonController {

	@Autowired private PersonDao dao;
	
	@GetMapping("/person")
	public List<Person> getAll(){
		return dao.findAll();
	}
	
	@GetMapping("/person/{id}")
	public Person getById(@PathVariable Long id){
		Optional<Person> person = dao.findById(id);
		return person.get();
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Person cretae(@Valid @RequestBody Person person){
		return dao.save(person);
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Person update(@RequestBody Person person){
		return dao.save(person);
	}
	
	@RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public void delete(@PathVariable Long id){
		dao.deleteById(id);
	}
}
