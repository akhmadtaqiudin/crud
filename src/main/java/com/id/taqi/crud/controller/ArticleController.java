package com.id.taqi.crud.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.id.taqi.crud.common.BaseException;
import com.id.taqi.crud.dao.ArticleDao;
import com.id.taqi.crud.model.Article;

import io.swagger.annotations.Api;

@Api(tags = "Modul Article")
@RestController @RequestMapping("/api")
public class ArticleController {

	@Autowired
	ArticleDao articleDao;
	
	@GetMapping("/article")
	public Page<Article> getAll(Pageable pageable){
		return articleDao.findAll(pageable);
	}
	
	@GetMapping("/article/{id}")
	public Article getById(@PathVariable("id") Article id){
		return id;
	}
	
	@PostMapping("/article")
	public Article createArticle(@Valid @RequestBody Article article){
		return articleDao.save(article);
	}
	
	@PutMapping("/article/{id}")
	public Article updateArticle(@PathVariable(value = "id") Long id, @Valid @RequestBody Article objarticle){
		
		Article article = articleDao.findById(id).orElseThrow(() -> new BaseException("Article dengan ", "id = ", id));
		article.setTitle(objarticle.toString());
		article.setContent(objarticle.getContent());
		
		Article updateArticle = articleDao.save(article);
		return updateArticle;
	}
	
	@DeleteMapping("/article/{id}")
	public ResponseEntity<?> deleteArticle(@PathVariable(value = "id") Long id){
		Article article = articleDao.findById(id).orElseThrow(() -> new BaseException("Article dengan ", "id = ", id));
		
		articleDao.delete(article);
		return ResponseEntity.ok().build();
	}
}
