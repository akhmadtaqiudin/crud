package com.id.taqi.crud.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.id.taqi.crud.model.Article;

@Repository
public interface ArticleDao extends PagingAndSortingRepository<Article, Long>{

}
