package com.id.taqi.crud.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.id.taqi.crud.model.Author;

public interface AuthorDao extends PagingAndSortingRepository<Author, String>{

}
