package com.id.taqi.crud.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.id.taqi.crud.model.Person;

public interface PersonDao extends JpaRepository<Person, Long>{
}
