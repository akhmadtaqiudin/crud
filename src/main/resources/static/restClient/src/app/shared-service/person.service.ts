import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Person } from '../person';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class PersonService {

  private baseUrl:string='http://localhost:8080/api';
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private person:Person;
  constructor(private _http:Http) { }

  errorHandler(error:Response){
    return Observable.throw(error||"SERVER ERROR");
  }
  getAll(){
    return this._http.get(this.baseUrl+'/person',this.options).map((response:Response)=>response.json()).catch(this.errorHandler);
  }
  
  getbyid(id:Number){
    return this._http.get(this.baseUrl+'/person/'+id,this.options).map((response:Response)=>response.json()).catch(this.errorHandler);
  }

  create(person:Person){
    return this._http.post(this.baseUrl+'/person',JSON.stringify,this.options).map((response:Response)=>response.json()).catch(this.errorHandler);
  }

  update(person:Person){
    return this._http.put(this.baseUrl+'/person',JSON.stringify,this.options).map((response:Response)=>response.json()).catch(this.errorHandler);
  }

  delete(id:Number){
    return this._http.delete(this.baseUrl+'/person/'+id,this.options).map((response:Response)=>response.json()).catch(this.errorHandler);
  }

  getter(){
    return this.person;
  }

  setter(person:Person){
    this.person=person;
  }
}
