import { Component, OnInit } from '@angular/core';
import { Person } from '../../person';
import { Router } from '@angular/router';
import { PersonService } from '../../shared-service/person.service';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {
  private person:Person;
  constructor(private _personService:PersonService, private _router:Router) { }

  ngOnInit() {
    this.person=this._personService.getter();
  }

  processSave(){
    if(this.person.id==undefined){
      this._personService.create(this.person).subscribe((person)=>{
        console.log(person);
        this._router.navigate(['/']);
      },(error)=>{
        console.log(error);
      });
    }else{
      this._personService.update(this.person).subscribe((person)=>{
        console.log(person);
        this._router.navigate(['/']);
      },(error)=>{
        console.log(error);
      });
    }
  }

}
