import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../shared-service/person.service';
import { Person } from '../../person';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listperson',
  templateUrl: './listperson.component.html',
  styleUrls: ['./listperson.component.css']
})
export class ListpersonComponent implements OnInit {
  private persons:Person[];
  constructor(private _personService:PersonService, private _router:Router) { }

  ngOnInit() {
    this._personService.getAll().subscribe((persons)=>{
      console.log(persons);
      this.persons=persons;
    },(error)=>{
      console.log(error);
    })
  }

  deletePerson(person){
    this._personService.delete(person.id).subscribe((data)=>{
      this.persons.splice(this.persons.indexOf(person),1);
    },(error)=>{
      console.log(error);
    });
  }

  updatePerson(person){
    this._personService.setter(person);
    this._router.navigate(['/op']);
  }

  addPerson(){
    let person = new Person();
    this._personService.setter(person);
    this._router.navigate(['/op']);
  }
}
