import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import { AppComponent } from './app.component';
import { ListpersonComponent } from './components/listperson/listperson.component';
import { PersonFormComponent } from './components/person-form/person-form.component';
import { PersonService } from './shared-service/person.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
const appRoutes:Routes=[
  {path:'',component:ListpersonComponent},
  {path:'op',component:PersonFormComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    ListpersonComponent,
    PersonFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PersonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
